import './../scss/styles.scss';
import jquery from 'jquery';
import 'slick-carousel';
import '@fancyapps/fancybox';
import 'jquery-form-styler';
import Inputmask from 'inputmask';

window.$ = jquery;
window.jQuery = jquery;

(function($) {
	Inputmask({mask: '+7 (999) 999-99-99', placeholder: '+7 (___) ___-__-__'}).mask('.phoneMask, [type="tel"]');

	$('.dropdown__header').on('click', function() {
		$(this).closest('.dropdown').toggleClass('opened');
		$(this).next().slideToggle(200);
		$(this).find('span').html(($(this).closest('.dropdown').hasClass('opened') ? 'Скрыть' : 'Показать') + '<i class="fa fa-chevron-down"></i>');
	});
	
	// Бургер меню
	$(".burger-menu").click(function () {
		$(this).toggleClass("active");
		$('.main-menu').fadeToggle(200);
	});
	
	
	// Выпадающее меню
	if ($(window).width() >= 991) {
		$('.drop').hover(function () {
			$(this).children('ul').stop(true, true).fadeToggle()
		})
	
	}
	
	if ($(window).width() < 991) {
		$('.drop .icon').click(function () {
			$(this).next('ul').stop(true, true).fadeToggle()
		})
	
	}
	
	$('.sendMessageForm').submit(function (e) {
		var data = {};
		$(this).find('input, textarea, select').each(function () {
			data[this.name] = $(this).val();
		});
		data.type = 1;
		$(".form-agreement label").css("color", "#626159");
		if ($("#agreement").prop('checked') || $("#agreement2").prop('checked')) {
			$.post('/local/templates/main/ajax/message.php', data, function (result) {
				$.fancybox.open($('#modal-thanks'));
			}, 'text');
		} else {
			$(".form-agreement label").css("color", "red");
		}
		e.preventDefault();
	});
	
	$('.sendMessageForm2').submit(function (e) {
		var data = {};
		$(this).find('input, textarea, select').each(function () {
			data[this.name] = $(this).val();
		});
		data.type = 2;
		$.post('/local/templates/main/ajax/message.php', data, function (result) {
			$.fancybox.close($('#modal-appointment'));
			$.fancybox.open($('#modal-thanks'));
		}, 'text');
		e.preventDefault();
	});
	
	// Баннер слайдер
	$('.banner').slick({
		dots: true,
		arrows: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
	});
	
	// Сллайдер команды
	$('.team-slider').slick({
		dots: false,
		arrows: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
	
	// Сллайдер докторов
	var doctorsSlider = $('.doctors-slider').slick({
		dots: false,
		arrows: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
	$('.doctors-slider__next').click(function () {
		$(doctorsSlider).slick("slickNext")
	});
	$('.doctors-slider__prev').click(function () {
		$(doctorsSlider).slick("slickPrev")
	});
	
	// Сллайдер сертификатов
	$('.certificate-slider').slick({
		dots: false,
		arrows: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
	
	// FansyBox
	$('.fancybox').fancybox({});
	
	// Акордеон
	$(".accordeon-item").click(function () {
		$(this).toggleClass("active");
		$(this).find(".accordeon-item__content").slideToggle(200);
	});
	
	//  Скролл вверх
	$('.footer-up').on('click', function (e) {
		e.preventDefault();
		$('html,body').animate({
			scrollTop: 0
		}, 700);
	});
	
	// Кнопка подробнее
	$(".doctor-list__more").click(function () {
		$(this).toggleClass("active");
		$(this).parent('.doctor-list').find('.doctor-list__hidden').fadeToggle()
	});
	
	window.Share = {
		vkontakte: function (purl, ptitle, pimg, text) {
			url = 'http://vkontakte.ru/share.php?';
			url += 'url=' + encodeURIComponent(purl);
			url += '&title=' + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&image=' + encodeURIComponent(pimg);
			url += '&noparse=true';
			Share.popup(url);
		},
		facebook: function (purl, ptitle, pimg, text) {
			url = 'http://www.facebook.com/sharer.php?s=100';
			url += '&p[title]=' + encodeURIComponent(ptitle);
			url += '&p[summary]=' + encodeURIComponent(text);
			url += '&p[url]=' + encodeURIComponent(purl);
			url += '&p[images][0]=' + encodeURIComponent(pimg);
			Share.popup(url);
		},
		popup: function (url) {
			window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
		}
	};
})(jquery)
